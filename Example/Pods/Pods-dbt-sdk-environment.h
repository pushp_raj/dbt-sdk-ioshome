
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// dbt-sdk
#define COCOAPODS_POD_AVAILABLE_dbt_sdk
#define COCOAPODS_VERSION_MAJOR_dbt_sdk 0
#define COCOAPODS_VERSION_MINOR_dbt_sdk 1
#define COCOAPODS_VERSION_PATCH_dbt_sdk 0

