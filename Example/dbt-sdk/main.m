//
//  main.m
//  dbt-sdk
//
//  Created by Brandon Trebitowski on 08/19/2014.
//  Copyright (c) 2014 Brandon Trebitowski. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
