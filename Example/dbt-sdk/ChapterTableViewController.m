//
//  ChapterTableViewController.m
//  DBT Test
//
//  Created by Brandon Trebitowski on 9/4/14.
//  Copyright (c) 2014 Treb Studios. All rights reserved.
//

#import "ChapterTableViewController.h"
#import "VerseViewController.h"
#import "DBT.h"
#import "DBTBook.h"
#import "DBTVolume.h"

@interface ChapterTableViewController ()
@property(nonatomic, strong) NSArray *chapters;
@end

@implementation ChapterTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = self.book.bookName;
    self.chapters = [self.book.chapters componentsSeparatedByString:@","];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.chapters.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSString *chapter = self.chapters[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"Chapter %@", chapter];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[self.volume.media lowercaseString] isEqualToString:@"text"]) {
        [self performSegueWithIdentifier:@"showDetail" sender:self];
    } else {
        [self performSegueWithIdentifier:@"showAudioDetail" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSString *chapter = self.chapters[indexPath.row];
        [[segue destinationViewController] setBook:self.book];
        [[segue destinationViewController] setChapter:chapter];
    } else if ([[segue identifier] isEqualToString:@"showAudioDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSString *chapter = self.chapters[indexPath.row];
        [[segue destinationViewController] setBook:self.book];
        [[segue destinationViewController] setChapter:chapter];
    }
}

@end
