//
//  ChapterTableViewController.h
//  DBT Test
//
//  Created by Brandon Trebitowski on 9/4/14.
//  Copyright (c) 2014 Treb Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DBTBook;
@class DBTVolume;

@interface ChapterTableViewController : UITableViewController
@property(nonatomic, strong) DBTBook *book;
@property(nonatomic, strong) DBTVolume *volume;
@end
